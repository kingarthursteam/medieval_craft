minetest.register_node("medieval_craft:medieval_glass_red", {
	description = "Red Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_red.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_orange", {
	description = "Orange Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_orange.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_yellow", {
	description = "Yellow Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_yellow.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_blue", {
	description = "Blue Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_blue.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_brown", {
	description = "Brown Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_brown.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_cyan", {
	description = "Cyan Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_cyan.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_darkgrey", {
	description = "Dark Grey Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_darkgrey.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_darkgreen", {
	description = "Dark Green Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_darkgreen.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_grey", {
	description = "Grey Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_grey.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_green", {
	description = "Green Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_green.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_magenta", {
	description = "Magenta Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_magenta.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_pink", {
	description = "Pink Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_pink.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_purple", {
	description = "Purple Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_purple.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

minetest.register_node("medieval_craft:medieval_glass_white", {
	description = "White Medieval Glass",
	drawtype = "glasslike",
	tiles = {"medieval_craft_medieval_glass_white.png"},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky=3,oddly_breakable_by_hand=3},
})

 -- crafts --
 
minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_red',
	recipe = {
		"dye:red",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_orange',
	recipe = {
		"dye:orange",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_yellow',
	recipe = {
		"dye:yellow",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_blue',
	recipe = {
		"dye:blue",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_brown',
	recipe = {
		"dye:brown",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_cyan',
	recipe = {
		"dye:cyan",
		"darkage:glass",
	}
})
 
  minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_darkgrey',
	recipe = {
		"dye:dark_grey",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_darkgreen',
	recipe = {
		"dye:darkgreen",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_green',
	recipe = {
		"dye:green",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_grey',
	recipe = {
		"dye:grey",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_darkgreen',
	recipe = {
		"dye:dark_green",
		"darkage:glass",
	}
})
minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_magenta',
	recipe = {
		"dye:magenta",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_pink',
	recipe = {
		"dye:pink",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_purple',
	recipe = {
		"dye:violet",
		"darkage:glass",
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:medieval_glass_white',
	recipe = {
		"dye:white",
		"darkage:glass",
	}
})

-- cooking -- 

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_red",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_yellow",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_orange",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_blue",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_brown",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_cyan",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_darkgrey",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_grey",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_darkgreen",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_green",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_magenta",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_pink",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_purple",
})

minetest.register_craft({
	type = "cooking",
	output = "darkage:glass",
	recipe = "medieval_craft:medieval_glass_white",
})
