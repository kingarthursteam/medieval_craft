GLASS_ALPHA = 60
GLASS_VISC = 1

-- Load other files --

dofile(minetest.get_modpath("medieval_craft").."/coloured_glass.lua")
dofile(minetest.get_modpath("medieval_craft").."/plaster_and_wood_bars.lua") 
dofile(minetest.get_modpath("medieval_craft").."/medieval_tools.lua")

-- the glass --

--glass removed, because its alredy in darkage

-- plaster and wood bars --

-- moved to "plaster_and_wood_bars.lua" file --

-- glowstone blocks --

minetest.register_node("medieval_craft:glow_stone", {
	description = "Glow Stone",
	drawtype = "glasslike",
	tiles = {"medieval_craft_glow_stone.png"},
	paramtype = "light",
	light_source = 18,
	sunlight_propagates = true,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("medieval_craft:glow_stone_lamp", {
	description = "Glow Stone Lamp",
	drawtype = "glasslike",
	tiles = {"medieval_craft_glow_stone_lamp.png"},
	paramtype = "light",
	light_source = 18,
	sunlight_propagates = true,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_glass_defaults(),
})

-- rope --

minetest.register_node("medieval_craft:rope", {
	description = "Rope",
	drawtype = "raillike",
	tiles = {"medieval_craft_rope.png", "medieval_craft_rope_curved.png", "medieval_craft_rope_junction.png", "medieval_craft_rope_crossing.png"},
	inventory_image = "medieval_craft_rope.png",
	wield_image = "medieval_craft_rope.png",
	paramtype = "light",
	walkable = false,
	selection_box = {
		type = "fixed",
                -- but how to specify the dimensions for curved and sideways rails?
                fixed = {-1/2, -1/2, -1/2, 1/2, -1/2+1/16, 1/2},
	},
	groups = {bendy=2,dig_immediate=2,attached_node=1},
})


minetest.register_craft({
	type = "shapeless",
	output = 'medieval_craft:rope',
	recipe = {
		"farming:string",
		"farming:string",
		"farming:string",
	}
})






