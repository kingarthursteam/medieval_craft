--[[
	plaster blocks
	==============
	
This blocks has been moved to the darkage mod.
Here we register only alias, for compatibility reasons

]]



minetest.register_alias("medieval_craft:plaster_arrow","darkage:reinforced_chalk_arrow")
minetest.register_alias("medieval_craft:plaster_bar","darkage:reinforced_chalk_left")
minetest.register_alias("medieval_craft:plaster_bars","darkage:reinforced_chalk_bars")

minetest.register_alias("medieval_craft:wood_arrow","darkage:reinforced_wood_arrow")
minetest.register_alias("medieval_craft:wood_bar","darkage:reinforced_wood_left")
minetest.register_alias("medieval_craft:wood_bars","darkage:reinforced_wood_bars")
