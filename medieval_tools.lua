minetest.register_tool("medieval_craft:sickle_wood", {
	description = "Wooden sickle",
	inventory_image = "medieval_craft_medieval_sickle_wood.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			snappy = {times={[2]=0.4, [3]=0.3}, uses=10, maxlevel=2},
		},
		damage_groups = {fleshy=2},
	},
})

minetest.register_tool("medieval_craft:sickle_stone", {
	description = "Stone sickle",
	inventory_image = "medieval_craft_medieval_sickle_stone.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			snappy = {times={[2]=0.2, [3]=0.2}, uses=20, maxlevel=2},
		},
		damage_groups = {fleshy=2},
	},
})

minetest.register_tool("medieval_craft:sickle_steel", {
	description = "Steel sickle",
	inventory_image = "medieval_craft_medieval_sickle_steel.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			snappy = {times={[2]=0.1, [3]=0.1}, uses=30, maxlevel=2},
		},
		damage_groups = {fleshy=2},
	},
})

minetest.register_craft({
	output = 'medieval_craft:sickle_wood',
	recipe = {
		{'group:wood', '', 'group:wood'},
		{'', 'group:wood', 'default:stick'},
		{'', '', 'default:stick'},
	}
})

minetest.register_craft({
	output = 'medieval_craft:sickle_stone',
	recipe = {
		{'group:stone', '', 'group:stone'},
		{'', 'group:stone', 'default:stick'},
		{'', '', 'default:stick'},
	}
})

minetest.register_craft({
	output = 'medieval_craft:sickle_steel',
	recipe = {
		{'default:steel_ingot', '', 'default:steel_ingot'},
		{'', 'default:steel_ingot', 'default:stick'},
		{'', '', 'default:stick'},
	}
})
